
IN_DEV_ENV := $(shell echo ${IN_DEV_ENV})

# enter dev_env only, if not already in it
ifeq ($(IN_DEV_ENV),1)
	DEV_ENV_SCRIPT :=
else
	DEV_ENV_SCRIPT := ./dev_env.sh
endif

help:
	printf "\n%s\n" "targets: tf_init, tf_plan, tf_apply, tf_destroy, docker_build, docker_build_no_cache, docker_push, ansible_install_requirements, ansible_create_user_vault, ansible_master_local, ansible_master_dev, dev_env, dev_env_build, dev_env_build_no_cache, dev_env_push\n"



# Infrastructure ############################################################

## terraform

tf_init: ## terraform init
	CRED_INJECTION=0 $(DEV_ENV_SCRIPT) "cd terraform; terraform init; cd .."

tf_plan: ## terraform plan
	CRED_INJECTION=0 $(DEV_ENV_SCRIPT) "cd terraform; terraform plan; cd .."

tf_apply: ## terraform apply
	CRED_INJECTION=0 $(DEV_ENV_SCRIPT) "cd terraform; terraform apply; cd .."

tf_destroy: ## terraform destroy
	CRED_INJECTION=0 $(DEV_ENV_SCRIPT) "cd terraform; terraform destroy; cd .."


## docker (for local setup)

### images

docker_build: ## build docker image
	.docker/images/.build.sh

docker_build_no_cache: ## build docker image without cache (force full rebuild)
	.docker/images/.build_no_cache.sh

docker_push: ## push docker image
	.docker/images/.push.sh

### compose
docker_compose_up: ## docker compose up
	cd .docker; docker-compose up -d; cd ..

docker_compose_down: ## docker compose down
	cd .docker; docker-compose down; cd ..



# software setup and state management ############################################################

## ansible

### scripts
ansible_install_requirements: ## install ansible requirements
	CRED_INJECTION=0 $(DEV_ENV_SCRIPT) "cd ansible; ansible-galaxy install -r requirements.yml; cd .."

ansible_create_user_vault: ## create user vault
	CRED_INJECTION=0 $(DEV_ENV_SCRIPT) "cd ansible; .scripts/create_user_vault.sh; cd .."

### ansible playbooks
ansible_master_local: ## runs ansible master playbook on local stage
	CRED_INJECTION=0 $(DEV_ENV_SCRIPT) "cd ansible; ansible-playbook -i hosts/local playbooks/master.yml; cd .."

ansible_master_dev: ## runs ansible master playbook on dev stage
	$(DEV_ENV_SCRIPT) "cd ansible; ansible-playbook -i hosts/dev -i user_vault.yml -i inventory.yml playbooks/master.yml; cd .."



# dev_env ############################################################

dev_env: ## runs and enters the DEV_ENV container running bash
	$(DEV_ENV_SCRIPT)

dev_env_build: ## build dev_env docker image
	.dev_env/.build.sh

dev_env_build_no_cache: ## build dev_env docker image without cache (force full rebuild)
	.dev_env/.build_no_cache.sh

dev_env_push: ## push dev_env docker image
	.dev_env/.push.sh
