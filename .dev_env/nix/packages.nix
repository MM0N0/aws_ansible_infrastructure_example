let
    fixed_pkgs = import ./pinned.nix;
in
[
    fixed_pkgs.busybox
    fixed_pkgs.gnumake
    fixed_pkgs.bash
    fixed_pkgs.acl
    fixed_pkgs.envsubst
    fixed_pkgs.sshpass

    fixed_pkgs.terraform

    fixed_pkgs.ansible_2_13
]
