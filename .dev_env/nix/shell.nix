let
    fixed_pkgs = import ./pinned.nix;
    fixed_packages = import ./packages.nix;
in
fixed_pkgs.mkShell {
    buildInputs = fixed_packages;

    # Env vars
    ANSIBLE_CONFIG=".docker/ansible.cfg";
    ANSIBLE_VAULT_PASS_FILE_user_vault="vault_pass_file";
}
