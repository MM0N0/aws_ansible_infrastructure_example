#!/usr/bin/env bash
SCRIPT_DIR=${0%/*}
source "${SCRIPT_DIR}/project.conf"

# build dev_env image
docker build -f "${SCRIPT_DIR}/Dockerfile" -t "mm0n0/dev_env_${PROJECT_NAME}:v1" "${SCRIPT_DIR}" --no-cache
