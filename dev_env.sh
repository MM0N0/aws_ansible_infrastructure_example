#!/usr/bin/env bash
SCRIPT_DIR=${0%/*}
source "${SCRIPT_DIR}/.dev_env/project.conf"

# run container non-interactively, if NO_TTY var is "1", default is "0"
INTERACTIVE_ARG=" -it"
if [ "${NO_TTY:-'0'}" == "1" ]; then
  INTERACTIVE_ARG=""
fi

# construct PRE_CMDS variable
PRE_CMDS="export PS1='[${PROJECT_NAME}] ';"
if [[ -n ${PROJECT_EXTRA_PRE_CMDS} ]]; then
  PRE_CMDS+="${PROJECT_EXTRA_PRE_CMDS};"
fi

# generate Env var statements for vault passwords
VAULT_IDS=$(cat "$SCRIPT_DIR/.dev_env/.ansible_vault_ids")
for vault_id in $VAULT_IDS
do
  ANSIBLE_VAULT_PASS_ENV_VARS="$ANSIBLE_VAULT_PASS_ENV_VARS ANSIBLE_VAULT_PASS_$vault_id=$(eval "cat \"\$ANSIBLE_VAULT_PASS_FILE_$vault_id\" 2> /dev/null")"
done

# run commands inside dev_env container
docker run --rm${INTERACTIVE_ARG} --net="host" --name="dev_env_${PROJECT_NAME}__container_$(date +%s)" \
  \
  -v "${PWD}":/workdir:rw \
  -v ~/.aws:/root/.aws \
  -v ~/.ansible:/root/.ansible \
  -v ~/.ssh:/root/.ssh \
  --mount type=tmpfs,destination=/ramdisk \
  ${PROJECT_EXTRA_DOCKER_ARGS} \
  "mm0n0/dev_env_${PROJECT_NAME}:v1" \
  \
  bash -c \
    "SSH_KEY_CONTENT='$(cat "$SSH_KEY_FILE" 2> /dev/null | tr "\n" "\t")' \
    ${ANSIBLE_VAULT_PASS_ENV_VARS} \
    CRED_INJECTION=${CRED_INJECTION:-'0'} .dev_env/.inject_ansible_credentials_then_run.sh \"${PRE_CMDS} ${1:-bash} $2 $3 $4 $5 $6 $7 $8 $9 ${10} ${11} ${12} ${13} ${14} ${15} ${16} ${17} ${18}\""
