variable "prefix" {}

variable "profile" {
    default = "default"
}

variable "region" {
  default = "eu-central-1"
}

#AWS Credentials
variable "shared_credentials_file" {}

#EC Config
variable "ec2_app_name" {}
variable "ec2_ssh_key_path" {}


#Route53 Config
variable "record_app_name" {}
variable "zone_id" {}
