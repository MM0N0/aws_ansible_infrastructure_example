resource "aws_vpc" "demo-vpc" {
  cidr_block       = "11.0.0.0/16"
  instance_tenancy = "default"
  enable_dns_hostnames = true
  assign_generated_ipv6_cidr_block = true

  tags = {
    Name = "demo-vpc"
  }
}

resource "aws_subnet" "app-private-subnet" {
  vpc_id     = aws_vpc.demo-vpc.id
  cidr_block = "11.0.1.0/24"
  availability_zone = "eu-central-1a"
}

resource "aws_subnet" "app-public-subnet" {
  vpc_id     = aws_vpc.demo-vpc.id
  cidr_block = "11.0.2.0/24"
  availability_zone = "eu-central-1a"
}

resource "aws_internet_gateway" "app-igw" {
  vpc_id = aws_vpc.demo-vpc.id
  
  tags = {
    Name = "app-igw"
  }
}

resource "aws_route_table" "app-public-route-table" {
  vpc_id = aws_vpc.demo-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.app-igw.id
  }

  tags = {
    Name = "app-public-route-table"
  }
}

resource "aws_route_table" "app-private-route-table" {
  vpc_id = aws_vpc.demo-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.app-igw.id
  }

  tags = {
    Name = "app-private-route-table"
  }
}

resource "aws_route_table_association" "app-public-route-table-association" {
  subnet_id      = aws_subnet.app-public-subnet.id
  route_table_id = aws_route_table.app-public-route-table.id
}

resource "aws_route_table_association" "app-private-route-table-association" {
  subnet_id      = aws_subnet.app-private-subnet.id
  route_table_id = aws_route_table.app-private-route-table.id
}
