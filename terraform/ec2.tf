/*
  This resource block defines an AWS security group for SSH access.
  It allows inbound traffic on port 22 from any IP address (0.0.0.0/0).
*/
resource "aws_security_group" "ssh" {
  vpc_id = aws_vpc.demo-vpc.id

  ingress {
    cidr_blocks      = ["0.0.0.0/0", ]
    description      = ""
    from_port        = 22
    ipv6_cidr_blocks = []
    prefix_list_ids  = []
    protocol         = "tcp"
    security_groups  = []
    self             = false
    to_port          = 22
  }
}

/**
 * This resource block defines an AWS security group for HTTP and HTTPS traffic.
 * It allows incoming traffic on port 80 (HTTP) and port 443 (HTTPS) from any IP address.
 */
resource "aws_security_group" "http" {
  vpc_id = aws_vpc.demo-vpc.id

  ingress {
    description      = ""
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  ingress {
    description      = ""
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

/*
  This resource block defines an AWS security group named "web_access".
  It allows all outbound traffic (egress) to any destination IP address (0.0.0.0/0) and any port.
*/
resource "aws_security_group" "web_access" {
  vpc_id = aws_vpc.demo-vpc.id

  egress {
    cidr_blocks      = ["0.0.0.0/0", ]
    description      = ""
    from_port        = 0
    ipv6_cidr_blocks = []
    prefix_list_ids  = []
    protocol         = "-1"
    security_groups  = []
    self             = false
    to_port          = 0
  }
}

/*
  This resource block defines an AWS EC2 instance for the Demo App.
  It specifies the AMI, instance type, key pair, security groups, and tags.
*/
resource "aws_instance" "app" {
  ami                         = "ami-0faab6bdbac9486fb"
  instance_type               = "t2.small"
  key_name                    = aws_key_pair.aws_key.key_name
  vpc_security_group_ids      = [aws_security_group.ssh.id, aws_security_group.http.id, aws_security_group.web_access.id]
  associate_public_ip_address = true
  subnet_id                   = aws_subnet.app-private-subnet.id

  tags = {
    Name = "${var.prefix}_${var.ec2_app_name}"
  }
}

resource "aws_key_pair" "aws_key" {
  key_name   = "${var.prefix}_aws_key"
  public_key = file(var.ec2_ssh_key_path)
}
