resource "aws_route53_record" "app" {
  name    = var.record_app_name
  type    = "A"
  ttl     = "60"
  zone_id = var.zone_id
  records = [aws_instance.app.public_ip]
}
