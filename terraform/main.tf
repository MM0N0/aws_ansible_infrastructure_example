terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.31.0"
    }
  }
}

provider "aws" {
  region                   = var.region
  shared_credentials_files = [var.shared_credentials_file]
  profile                  = var.profile
}

output "ec2_app_ip" {
  value = aws_instance.app.public_ip
}

# create inventory file for ansible
resource "local_file" "ansible_inventory" {
  content  = <<-EOT

    all:
      vars:
        ansible_user: ubuntu
        record_app_name: ${var.record_app_name}
      children:
        dev:
          hosts:
            dev_web_app:
              ansible_host: ${aws_instance.app.public_ip}

  EOT

  filename = "../ansible/inventory.yml"
}
