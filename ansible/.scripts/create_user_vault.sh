#!/usr/bin/env bash
function encrypt_string() {
  VALUE_NAME=$1

  printf "____________________________________\n"
  printf "TYPE value for %s:\n" "$VALUE_NAME"
  echo -n "$VALUE_NAME":
  read -s VALUE_CLEAR
  printf "\n"

  ENCRYPTED_VALUE=$(ansible-vault encrypt_string --vault-id user_vault@$VAULT_PASS_PATH "$VALUE_CLEAR" 2>&1 | sed -e 's/\x1b\[.\{1,5\}m//g' | sed -E '/^[^ ].*/d' | sed -e 's/ /   /g')
  export ENCRYPTED_VALUE="$(printf '!vault |\n%s' "$ENCRYPTED_VALUE")"
}
function ask_for_and_encrypt() {
  NAME_OF_VALUE_TO_ENCRYPT=$1

  encrypt_string "$NAME_OF_VALUE_TO_ENCRYPT"
  eval "export $NAME_OF_VALUE_TO_ENCRYPT=\$ENCRYPTED_VALUE"
}
function gen_values_to_encrypt() {
  TEMPLATE_FILE=$1

  grep -F "\$" "$TEMPLATE_FILE" | sed -E 's/ *[^:]+: \$([^ ]+)/\1/g'
}

SCRIPT_DIR=${0%/*}
VAULT_PASS_PATH='/ramdisk/vault_pass'
TEMPLATE_FILE='user_vault.yml.template'
USER_VAULT_FILE='user_vault.yml'

# don't generate if $USER_VAULT_FILE already exists
if test -f $USER_VAULT_FILE; then
  printf "'%s' already exists. \ndelete it, if you want to generate it.\n" "$USER_VAULT_FILE"
  exit 0
fi

printf "____________________________________\n"
printf "TYPE Vault-PASSWORD:\n"
echo -n Vault-Password:
read -s VAULT_PASS
printf "\n"
echo "$VAULT_PASS" > $VAULT_PASS_PATH

# find all ENV vars in $TEMPLATE_FILE and prompt user for them
VALUES_TO_ENCRYPT=$(gen_values_to_encrypt $TEMPLATE_FILE)
for value in $VALUES_TO_ENCRYPT
do
   echo "val: $value"
   eval "ask_for_and_encrypt $value"
done
printf "____________________________________\n"

echo "creating '$USER_VAULT_FILE'"
envsubst < $TEMPLATE_FILE > $USER_VAULT_FILE

echo "done"
