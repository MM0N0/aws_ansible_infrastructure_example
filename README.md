

# aws ansible infrastructure example Project
this is a simple project to showcase how to use aws (configured with terraform) and ansible to manage cloud server. 

**Note**: this is work in progress

## Requirements
- docker and docker-compose
- bash
- cat

note: if you are using windows, you will have to use wsl


## TODOs
- cleanup documentation
- check local "localhost_group" in user_vault.yml, can it be moved to "all.yml"?

## Contribute
...

## License

GNU General Public License v3.0 or later

See [COPYING](COPYING) to see the full text.
